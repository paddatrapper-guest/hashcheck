project(hashcheck CXX)
cmake_minimum_required(VERSION 3.1)

include(GNUInstallDirs)

set(HASHCHECK_VERSION_MAJOR 1)
set(HASHCHECK_VERSION_MINOR 0)
set(HASHCHECK_VERSION_PATCH 0)

set(HASHCHECK_VERSION ${HASHCHECK_VERSION_MAJOR}.${HASHCHECK_VERSION_MINOR}.${HASHCHECK_VERSION_PATCH})

set(CMAKE_AUTOMOC ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_CXX_FLAGS "${CXX_FLAGS} -Wall")
set(CMAKE_CXX_STANDARD 11)
set(EXECUTABLE_NAME "hashcheck")

add_definitions("-DHASHCHECK_VERSION=\"${HASHCHECK_VERSION}\"")

find_package(Qt5 REQUIRED Core Widgets)

set(SOURCES
    src/main.cpp
    src/main_window.cpp
)

set(UI
    src/main_window.ui
)

qt5_wrap_ui(UI_SOURCE ${UI})
list(APPEND SOURCES ${UI_SOURCE})

add_executable(${EXECUTABLE_NAME} ${SOURCES})
qt5_use_modules(${EXECUTABLE_NAME} Core Widgets)

install(TARGETS ${EXECUTABLE_NAME} RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})
install(FILES hashcheck.desktop DESTINATION ${CMAKE_INSTALL_DATADIR}/applications)
install(FILES icon.png DESTINATION ${CMAKE_INSTALL_DATADIR}/pixmaps RENAME hashcheck.png)
